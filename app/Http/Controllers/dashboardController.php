<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function index(){
        return view('back.content.home');
    }
    public function datatables(){
        return view('back.content.datatables');
    }
}